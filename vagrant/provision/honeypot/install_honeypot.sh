#!/bin/bash

# Initialize
/vagrant/init_system.sh

# Configure DMZ_2 interface
sed -i 's/allow-hotplug eth0/auto eth0/g' /etc/network/interfaces

echo "" >> /etc/network/interfaces
echo "# DMZ_2" >> /etc/network/interfaces
echo "auto eth1" >> /etc/network/interfaces
echo "iface eth1 inet static" >> /etc/network/interfaces
echo "  address 192.168.90.2" >> /etc/network/interfaces
echo "  netmask 255.255.255.0" >> /etc/network/interfaces
echo "  gateway 192.168.90.1" >> /etc/network/interfaces
echo "  dns-nameservers 192.168.90.1 1.1.1.1 8.8.8.8" >> /etc/network/interfaces

# Configure DNS
# TODO: ELIMINAR CUANDO SE PUEDA
#sed -i '/nameserver/d' /etc/resolv.conf
sudo sed -i '1s/^/nameserver 192.168.90.1\n/' /etc/resolv.conf
sed -i 's/search home/search blueteam.local/g' /etc/resolv.conf
sed -i 's/domain home/domain blueteam.local/g' /etc/resolv.conf

# Install Docker
/vagrant/install_docker.sh

# Install Suricata
/vagrant/install_suricata.sh

# Install ELK agent
/vagrant/install_elastic_agent.sh

# Run SSH Honey Pot
docker run --restart always -p 3333:2222 -d --name honeypot cowrie/cowrie:22bd5430

# Configure traffic route to LAN on next boot
sudo /vagrant/runonce.sh "ip route add 192.168.100.0/24 via 192.168.90.1"
# Configure traffic route to DMZ on next boot
sudo /vagrant/runonce.sh "ip route add 192.168.200.0/24 via 192.168.90.1"
# Configure traffic route to WAN on next boot
sudo /vagrant/runonce.sh "ip route del default"
sudo /vagrant/runonce.sh "ip route add default via 192.168.90.1"

# Clean system
#/home/devops/provision/purge.sh

exit 0
