#!/bin/bash

# Set shell apareance
echo 'force_color_prompt=yes' | sudo tee -a /etc/bash.bashrc

# Set timezone
timedatectl set-timezone Europe/Madrid

# Create "devops" user
useradd -m -s /bin/bash -U devops
cp -pr /home/vagrant/.ssh /home/devops/
echo 'devops:devops' | chpasswd
echo '%devops ALL=(ALL) NOPASSWD: ALL' | sudo tee -a /etc/sudoers.d/devops

# Access with password
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd

exit 0
