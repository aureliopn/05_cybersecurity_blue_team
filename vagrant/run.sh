#!/bin/bash

vagrant up utm > utm_up.log
vagrant up siem > siem_up.log

echo "_________________________________________________________________________________________________________"
echo "1. Log in 'http://localhost:5601/' (ckeck port if you have changed it) using user 'elastic' and password:"
cat ./provision/siem/elastic_password.txt
echo "2. Add Suricata Events integrations."
echo "3. Save the standalone configuration file to '.\provision\elastic-agent.yml'"
echo "_________________________________________________________________________________________________________"
echo "Press ENTER when done to continue..."

vagrant up honeypot > honeypot_up.log
vagrant up workstation > workstation_up.log

vagrant reload > -file reload.log

exit 0
