#!/bin/bash

# Initialize
/vagrant/init_system.sh

# Configure DMZ interface
sed -i 's/allow-hotplug eth0/auto eth0/g' /etc/network/interfaces

echo "" >> /etc/network/interfaces
echo "# DMZ" >> /etc/network/interfaces
echo "auto eth1" >> /etc/network/interfaces
echo "iface eth1 inet static" >> /etc/network/interfaces
echo "  address 192.168.200.2" >> /etc/network/interfaces
echo "  netmask 255.255.255.0" >> /etc/network/interfaces
echo "  gateway 192.168.200.1" >> /etc/network/interfaces
echo "  dns-nameservers 192.168.200.1 1.1.1.1 8.8.8.8" >> /etc/network/interfaces

# Configure DNS
# TODO: ELIMINAR CUANDO SE PUEDA
#sed -i '/nameserver/d' /etc/resolv.conf
sudo sed -i '1s/^/nameserver 192.168.200.1\n/' /etc/resolv.conf
sed -i 's/search home/search blueteam.local/g' /etc/resolv.conf
sed -i 's/domain home/domain blueteam.local/g' /etc/resolv.conf

# Install Docker
/vagrant/install_docker.sh

# Install Docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Set vm.max_map_count to Elasticsearch
echo 'vm.max_map_count=262144' | sudo tee -a /etc/sysctl.conf
sysctl -w vm.max_map_count=262144

# Start ELK
cd /home/devops/
cp -R /vagrant/docker-elk ./
chown devops:devops /home/devops -R
cd docker-elk
docker-compose up -d
echo "Starting ELK..."
until [[ $(docker logs docker-elk_kibana_1 | grep "Fleet setup completed") ]]; do
  echo -n "."
  sleep 1
done
echo ""
echo "Changing 'elastic' user password..."
PASSWORD_CHANGING_RESULT=""
until [[ "${PASSWORD_CHANGING_RESULT}" == *"New value"* ]]; do
  echo -n "."
  sleep 1
  echo "${PASSWORD_CHANGING_RESULT}" > /vagrant/siem/elastic_password_debug.txt
  PASSWORD_CHANGING_RESULT=$(docker exec -t docker-elk_elasticsearch_1 bin/elasticsearch-reset-password --batch --user elastic)
done
echo "${PASSWORD_CHANGING_RESULT}" | tail -n 1 | awk -F " " '{print $3}' > /vagrant/siem/elastic_password.txt
echo ""
echo "ELK ready."

# Configure traffic route to LAN on next boot
sudo /vagrant/runonce.sh "ip route add 192.168.100.0/24 via 192.168.200.1"
# Configure traffic route to DMZ_2 on next boot
sudo /vagrant/runonce.sh "ip route add 192.168.90.0/24 via 192.168.200.1"
# Configure traffic route to WAN on next boot
sudo /vagrant/runonce.sh "ip route del default"
sudo /vagrant/runonce.sh "ip route add default via 192.168.200.1"

# Clean system
#/home/devops/provision/purge.sh

exit 0
