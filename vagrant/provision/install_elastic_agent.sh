#!/bin/bash

cp /vagrant/elastic-agent-8.0.1-linux-x86_64.tar.gz /home/devops/elastic-agent-8.0.1-linux-x86_64.tar.gz
cd /home/devops
tar -zxvf ./elastic-agent-8.0.1-linux-x86_64.tar.gz
rm -rf ./elastic-agent-8.0.1-linux-x86_64.tar.gz

cp /vagrant/elastic-agent.yml /home/devops/elastic-agent-8.0.1-linux-x86_64/elastic-agent.yml
PASSWORD=$(head -n 1 /vagrant/siem/elastic_password.txt)
PASSWORD="${PASSWORD/$'\r'/}"
CONFIG_FILE=$(</home/devops/elastic-agent-8.0.1-linux-x86_64/elastic-agent.yml)
CONFIG_FILE="${CONFIG_FILE//localhost/192.168.200.2}"
CONFIG_FILE="${CONFIG_FILE//{ES_USERNAME\}/elastic}"
CONFIG_FILE="${CONFIG_FILE//{ES_PASSWORD\}/$PASSWORD}"
echo "$CONFIG_FILE" > /home/devops/elastic-agent-8.0.1-linux-x86_64/elastic-agent.yml
cd elastic-agent-8.0.1-linux-x86_64
sudo chown devops:devops /home/devops/elastic-agent-8.0.1-linux-x86_64 -R
( sleep 1 ; printf "y\r\n"; sleep 1; printf "n\r\n"; ) | sudo ./elastic-agent install

exit 0
