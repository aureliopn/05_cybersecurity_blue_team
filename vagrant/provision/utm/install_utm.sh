#!/bin/bash

WAN=eth0
LAN=eth1
DMZ=eth2
DMZ_2=eth3
VPN=tun0
SIEM=192.168.200.2
SIEM_PORT=9200
HONEY_POT=192.168.90.2
LAN_SUBNET=192.168.100
DMZ_SUBNET=192.168.200
DMZ_2_SUBNET=192.168.90
VPN_PORT=1194
SSH_PORT=22

# Initialize
/vagrant/init_system.sh

# Configure network interfaces
sed -i 's/allow-hotplug eth0/auto eth0/g' /etc/network/interfaces

echo "" >> /etc/network/interfaces
echo "# LAN" >> /etc/network/interfaces
echo "auto ${LAN}" >> /etc/network/interfaces
echo "iface ${LAN} inet static" >> /etc/network/interfaces
echo "  address ${LAN_SUBNET}.1" >> /etc/network/interfaces
echo "  netmask 255.255.255.0" >> /etc/network/interfaces
echo "  network ${LAN_SUBNET}.0" >> /etc/network/interfaces
echo "  broadcast ${LAN_SUBNET}.255" >> /etc/network/interfaces

echo "" >> /etc/network/interfaces
echo "# DMZ" >> /etc/network/interfaces
echo "auto ${DMZ}" >> /etc/network/interfaces
echo "iface ${DMZ} inet static" >> /etc/network/interfaces
echo "  address ${DMZ_SUBNET}.1" >> /etc/network/interfaces
echo "  netmask 255.255.255.0" >> /etc/network/interfaces
echo "  network ${DMZ_SUBNET}.0" >> /etc/network/interfaces
echo "  broadcast ${DMZ_SUBNET}.255" >> /etc/network/interfaces

echo "" >> /etc/network/interfaces
echo "# DMZ_2" >> /etc/network/interfaces
echo "auto ${DMZ_2}" >> /etc/network/interfaces
echo "iface ${DMZ_2} inet static" >> /etc/network/interfaces
echo "  address ${DMZ_2_SUBNET}.1" >> /etc/network/interfaces
echo "  netmask 255.255.255.0" >> /etc/network/interfaces
echo "  network ${DMZ_2_SUBNET}.0" >> /etc/network/interfaces
echo "  broadcast ${DMZ_2_SUBNET}.255" >> /etc/network/interfaces

# Copy Easy RSA to home directory
cd /home/devops
cp /vagrant/utm/EasyRSA-unix-v3.0.6.tgz ./
# Install Easy-RSA for CA server
tar -xzvf ./EasyRSA-unix-v3.0.6.tgz
mv ./EasyRSA-v3.0.6 ./ca-server
cd ca-server
cp /vagrant/utm/vars ./
./easyrsa init-pki
./easyrsa build-ca nopass <<EOF
utm-ca
EOF
cd ..
# Install Easy-RSA for Open VPN server
tar -xzvf ./EasyRSA-unix-v3.0.6.tgz
mv ./EasyRSA-v3.0.6 ./ca-openvpn
cd ca-openvpn
./easyrsa init-pki
./easyrsa gen-req server nopass <<EOF
server
EOF
cd ..
# Delete EasyRSA installation package
rm ./EasyRSA-unix-v3.0.6.tgz

# Install OpenVPN
sudo apt-get update
sudo apt-get install openvpn -y

# Copy OpenVPN generated certificate key
sudo cp ./ca-openvpn/pki/private/server.key /etc/openvpn/server.key
# Copy Open VPN certificate request to CA server
cp ./ca-openvpn/pki/reqs/server.req ./ca-server/server.req
# Sign Open VPN certificate request with CA server
cd ca-server
./easyrsa import-req ./server.req server
./easyrsa sign-req server server <<EOF
yes
EOF
cd ..

# Copy OpenVPN signed certificate and CA certificate
sudo cp ./ca-server/pki/issued/server.crt /etc/openvpn/server.crt
# Copy CA certificate
sudo cp ./ca-server/pki/ca.crt /etc/openvpn/ca.crt

# Use strong Diffie-Hellman key to use during Open VPN key exchange
cd ca-openvpn
./easyrsa gen-dh
sudo openvpn --genkey secret ta.key
sudo cp ./ta.key /etc/openvpn/ta.key
sudo cp ./pki/dh.pem /etc/openvpn/dh.pem
# Create key pair to be used in Open VPN clients
./easyrsa gen-req devops nopass <<EOF
devops
EOF
cd ..

# Initialize client keys repository
mkdir -p ./client-configs/keys
mkdir -p ./client-configs/files
cp /vagrant/utm/base.conf ./client-configs/base.conf
cp /vagrant/utm/make_config.sh ./client-configs/make_config.sh
chmod -R 700 ./client-configs
# Copy client certificate key to client keys repository
cp ./ca-openvpn/pki/private/devops.key ./client-configs/keys/devops.key

# Sign Open VPN client certificate request with CA server
cp ./ca-openvpn/pki/reqs/devops.req ./ca-server/devops.req
cd ca-server
./easyrsa import-req ./devops.req devops
./easyrsa sign-req client devops <<EOF
yes
EOF
cd ..
# Copy client signed certificate to client keys repository
cp ./ca-server/pki/issued/devops.crt ./client-configs/keys/devops.crt

# Copy Diffie-Hellman key to client keys repository
sudo cp ./ca-openvpn/ta.key ./client-configs/keys/ta.key
# Copy CA certificate to client keys repository
cp ./ca-server/pki/ca.crt ./client-configs/keys/ca.crt

# Generate client config configuration
chown devops:devops /home/devops -R
cd client-configs
./make_config.sh devops
cp ./files/devops.ovpn /vagrant/utm/devops.ovpn
cd ..

# Start Open VPN server service
sudo cp /vagrant/utm/server.conf /etc/openvpn/server.conf
sudo systemctl start openvpn@server
sudo systemctl enable openvpn@server

# Enable port forwarding
sudo sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sudo bash -c 'echo 1 >> /proc/sys/net/ipv4/ip_forward'
sudo sysctl -p

# Initialize iptables
sudo iptables -F
sudo iptables -X

# Enable traffic on localhost
sudo iptables -A INPUT -i lo -j ACCEPT
# Drop input traffic from DMZ and DMZ_2
sudo iptables -A INPUT -i $DMZ -m state --state NEW -j DROP
sudo iptables -A INPUT -i $DMZ_2 -m state --state NEW -j DROP
# Drop new connections from DMZ to other interfaces
sudo iptables -A FORWARD -i $DMZ -m state --state NEW -j DROP
# Drop new connections from DMZ_2 to other interfaces except traffic from Honey Pot to SIEM on port 9200
sudo iptables -A FORWARD -i $DMZ_2 -o $DMZ -s $HONEY_POT -p tcp --dport $SIEM_PORT -j ACCEPT
sudo iptables -A FORWARD -i $DMZ_2 -m state --state NEW -j DROP

# Config iptables
sudo iptables -A FORWARD -i $WAN -o $VPN -j ACCEPT
sudo iptables -A FORWARD -i $VPN -o $WAN -j ACCEPT
sudo iptables -A FORWARD -i $LAN -o $VPN -j ACCEPT
sudo iptables -A FORWARD -i $VPN -o $LAN -j ACCEPT
sudo iptables -A FORWARD -i $WAN -o $LAN -j ACCEPT
sudo iptables -A FORWARD -i $LAN -o $WAN -j ACCEPT
sudo iptables -A INPUT -i $VPN -j ACCEPT
sudo iptables -A INPUT -i $LAN -j ACCEPT
sudo iptables -A INPUT -i $WAN -p udp --dport $VPN_PORT -j ACCEPT
sudo iptables -A INPUT -i $WAN -p tcp --dport $SSH_PORT -j ACCEPT
sudo iptables -A INPUT -i $WAN -m state --state NEW -j DROP
sudo iptables -t nat -A POSTROUTING -o $WAN -j MASQUERADE
sudo iptables -t nat -A POSTROUTING -o $VPN -j MASQUERADE

# Save firewall rules
sudo mkdir -p /etc/iptables
sudo /bin/bash -c "iptables-save > /etc/iptables/rules.v4"
sudo cp /vagrant/utm/iptables /etc/network/if-pre-up.d/iptables

# Clean system
#/home/devops/provision/purge.sh

exit 0
