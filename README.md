# Blue Team - Bootcamp Cybersecurity III

Proyecto académico con el objetivo de aplicar las técnicas y herramientas vistas durante el curso para montar la siguiente infraestructura:

![Infrastructure](./img/01-infrastructure.png)

## Objetivos

* Máquina UTM en bridge que conecte 3 redes internas:
  * LAN.
  * DMZ.
  * DMZ_2.
* Máquina SIEM en subred DMZ con un stack ELK.
* Máquina Honeypot en subred DMZ_2 con un servicio fake SSH que publique los logs en el stack ELK.
* Máquina Workstation en subred LAN que:
  * Publique los logs en el stack ELK.
  * Sea accesible sólo por VPN.

## Software de las máquinas virtuales

* UTM:
  * [Easy-RSA](https://github.com/OpenVPN/easy-rsa).
  * [OpenVPN](https://openvpn.net/).
  * [Iptables](https://linux.die.net/man/8/iptables).
* Workstation:
  * [Suricata](https://suricata.io/).
  * [Elastic agent](https://docs.elastic.co/en/integrations/suricata).
* SIEM:  
  * [Docker](https://www.docker.com/).
  * [Docker-compose](https://docs.docker.com/compose/).
  * [ELK](https://github.com/deviantony/docker-elk).
* Honeypot:
  * [Docker](https://www.docker.com/).
  * [SSH (Honeypot)](https://hub.docker.com/r/cowrie/cowrie).
  * [Suricata](https://suricata.io/).
  * [Elastic agent](https://docs.elastic.co/en/integrations/suricata).

## Networking

* WAN:
  * Sólo se permite el tráfico entrante a las subredes DMZ y DMZ_2.
* LAN:
  * No se permite el tráfico entrante desde ninguna otra red.
  * Se puede acceder a través de VPN.
* DMZ:
  * No se permite el tráfico entrante desde la subred DMZ_2, a excepción de la publicación de logs a través del puerto 9200.
  * Accesible desde el resto de subredes.
* DMZ_2:
  * Accesible desde todas las redes.

## Software de la máquina host

Se han utilizado los siguientes sistemas operativos y herramientas:

* [Windows](https://www.microsoft.com/es-es/software-download/windows10) 10.
  * [VirtualBox](https://www.virtualbox.org/) 6.1.32 r149290 (Qt5.6.2).
  * [Vagrant](https://www.vagrantup.com/) 2.2.19.
  * [ScreenToGif](https://www.screentogif.com/) 2.35.4.

## Instrucciones para desplegar la infraestructura

Clonar este repositorio y ejecutar en el directorio [\vagrant](./vagrant/):

* Powershell: [\run.ps1](./vagrant/run.ps1).
* Bash (dar permisos de ejecución): [\run.sh](./vagrant/run.sh) (sin probar).

Realizar las siguientes acciones cuando lo solicite el script:

* Entrar en el Kibana que se ha levantado en la máquina SIEM.
* Añadir la integración con Suricata.
* Exportar el archivo de configuración para los agentes de Elasticsearch.
  * Guardar en el archivo en [\vagrant\provision\elastic-agent.yml](./vagrant/provision/elastic-agent.yml).
* Pulsar la tecla INTRO.
* Esperar a que finalice la ejecución del script.

![Run](./img/02-run.gif)

Una vez terminada la ejecución del script se puede ver el resultado de las operaciones en los archivos generados de log:

* [utm_up.log](./vagrant/utm_up.log).
* [siem_up.log](./vagrant/siem_up.log).
* [honeypot_up.log](./vagrant/honeypot_up.log).
* [workstation_up.log](./vagrant/workstation_up.log).
* [reload.log](./vagrant/reload.log).

## Observabilidad

En Kibana se puede ver la ingesta de los logs de las máquinas "Honeypot" y "Workstation":

![Run](./img/03-observability_overview.png)

## VPN

Pasos para probar el acceso por VPN:

* Apagar la máquina UTM.
* Seleccionar la máquina en Virtual Box.
* Ir a Settings --> Networking --> Adapter 1.
* Seleccionar "Attached to: Bridged Adapter" con el adaptador de nuestra máquina host que tiene salidda al router.
* Aceptar los cambios.
* Arrancar la máquina UTM.
* Identificar la IP que el router le ha dado a la máquina UTM.
* Cambiar la línea ```remote my-server-1 1194``` por ```remote <IP> 1194``` en el archivo [\vagrant\provision\utm\devops.ovpn](./vagrant/provision/utm/devops.ovpn).
* Abrir el archivo [\vagrant\provision\utm\devops.ovpn](./vagrant/provision/utm/devops.ovpn) modificado con el cliente de Open VPN.
* Conectar con a la VPN.
* Probar la conexión por SSH con la máquina Workstation ejecutando ```ssh 192.168.100.10```.

![Run](./img/04-open_vpn.gif)

## Scripts

* [run.ps1](./vagrant/run.ps1) o [run.sh](./vagrant/run.sh).
  * [Vagrantfile](./vagrant/Vagrantfile).
    * [install_utm.sh](./vagrant/provision/utm/install_utm.sh).
      * [init_system.sh](./vagrant/provision/init_system.sh).
      * [iptables](./vagrant/provision/utm/iptables).
      * [make_config.sh](./vagrant/provision/utm/make_config.sh).
    * [install_siem.sh](./vagrant/provision/siem/install_siem.sh).
      * [init_system.sh](./vagrant/provision/init_system.sh).
      * [install_docker.sh](./vagrant/provision/install_docker.sh).
      * [runonce.sh](./vagrant/provision/runonce.sh).
    * [install_honeypot.sh](./vagrant/provision/honeypot/install_honeypot.sh).
      * [init_system.sh](./vagrant/provision/init_system.sh).
      * [install_docker.sh](./vagrant/provision/install_docker.sh).
      * [install_suricata.sh](./vagrant/provision/install_suricata.sh).
      * [install_elastic_agent.sh](./vagrant/provision/install_elastic_agent.sh).
      * [runonce.sh](./vagrant/provision/runonce.sh).
    * [install_workstation.sh](./vagrant/provision/workstation/install_workstation.sh).
      * [init_system.sh](./vagrant/provision/init_system.sh).
      * [install_suricata.sh](./vagrant/provision/install_suricata.sh).
      * [install_elastic_agent.sh](./vagrant/provision/install_elastic_agent.sh).

### Script "run.ps1" o "run.sh"

El script [run.ps1](./vagrant/run.ps1) o [run.sh](./vagrant/run.sh) es el punto de entrada para levantar la infraestructura. Lo que hace es:

* Levantar máquina UTM con Vagrant.
* Levantar máquina SIEM con Vagrant.
* Interactuar con el usuario para que guarde el archivo de configuración del agente de Elasticsearch en [\vagrant\provision\elastic-agent.yml](./vagrant/provision/elastic-agent.yml).
* Levantar máquina Honeypot con Vagrant.
* Levantar máquina Workstation con Vagrant.
* Reiniciar todas las máquinas con Vagrant para que se actualicen los enrutados ejecutando el script [runonce.sh](./vagrant/provision/runonce.sh) después del reinicio de cada máquina.

### Script "Vagranfile"

El archivo [Vagrantfile](./vagrant/Vagrantfile) define las máquinas virtuales con Vagrant. Las máquinas y sus características son:

* UTM:
  * Interfaces de red:
    * LAN.
    * DMZ.
    * DMZ_2.
  * Acceso por SSH desde el host con NAT en el puerto 2222.
  * Script de aprovisionamiento al arrancar por primera vez la máquina:
    * [install_utm.sh](./vagrant/provision/utm/install_utm.sh).
* SIEM:
  * Interfaces de red:
    * DMZ.
  * Acceso por SSH desde el host con NAT en el puerto 2223.
  * Script de aprovisionamiento al arrancar por primera vez la máquina:
    * [install_siem.sh](./vagrant/provision/siem/install_siem.sh).
* Honeypot:
  * Interfaces de red:
    * DMZ_2.
  * Acceso por SSH desde el host con NAT en el puerto 2224.
  * Script de aprovisionamiento al arrancar por primera vez la máquina:
    * [install_honeypot.sh](./vagrant/provision/honeypot/install_honeypot.sh).
* Workstation:
  * Interfaces de red:
    * LAN.
  * Acceso por SSH desde el host con NAT en el puerto 2225.
  * Script de aprovisionamiento al arrancar por primera vez la máquina:
    * [install_workstation.sh](./vagrant/provision/workstation/install_workstation.sh).

### Script "install_utm.sh"

El script [install_utm.sh](./vagrant/provision/utm/install_utm.sh) se ejecuta una única vez en el primer arranque de la máquina virtual con Vagrant. Lo que hace es:

* Inicializar el sistema con [init_system.sh](./vagrant/provision/init_system.sh).
* Configurar las interfaces de red. La configuración no se puede aplicar inmediatamente porque se interrumpe la interacción con Vagrant al reiniciar las interfaces de red. Los cambios estarán operativos después del siguiente inicio del sistema.
* Crear CA Server con Easy-RSA:
  * Instalar el [paquete de Easy-RSA](./vagrant/provision/utm/EasyRSA-unix-v3.0.6.tgz).
  * Configurarlo con el archivo [](./vagrant/provision/utm/vars).
* Instalar Easy-RSA para OpenVPN:
  * Instalar el [paquete de Easy-RSA](./vagrant/provision/utm/EasyRSA-unix-v3.0.6.tgz).
* Instalar OpenVPN con la configuración del archivo [server.conf](./vagrant/provision/utm/server.conf).
* Configurar los certificados para OpenVPN con el CA Server.
* Utilizar [base.conf](./vagrant/provision/utm/base.conf) y [make_config](./vagrant/provision/utm/make_config.sh) para crear el archivo de configuración para conexión con un cliente de OpenVPN. El archivo de configuración se guarda en [\vagrant\provision\utm\devops.ovpn](./vagrant/provision/utm/devops.ovpn).
* Arrancar OpenVPN.
* Configurar el firewall con "iptables".
* Utilizar el script [iptables](./vagrant/provision/utm/iptables) para que las reglas del firewall se carguen en cada reinicio del sistema. No se utiliza el paquete [iptables-persistent](https://packages.debian.org/sid/iptables-persistent) para esta funcionalidad porque requiere interacción del usuario durante la instalalción.

### Script "install_siem.sh"

El script [install_siem.sh](./vagrant/provision/siem/install_siem.sh) se ejecuta una única vez en el primer arranque de la máquina virtual con Vagrant. Lo que hace es:

* Inicializar el sistema con [init_system.sh](./vagrant/provision/init_system.sh).
* Configurar las interfaces de red. La configuración no se puede aplicar inmediatamente porque se interrumpe la interacción con Vagrant al reiniciar las interfaces de red. Los cambios estarán operativos después del siguiente inicio del sistema.
* Instalar Docker con [install_docker.sh](./vagrant/provision/install_docker.sh).
* Instalar Docker-compose.
* Arrancar el [stack de ELK](./vagrant/provision/docker-elk/) con Docker-compose.
* Cambiar la contraseña por defecto del usuario "*elastic*" y guardarla en el archivo [\vagrant\provision\siem\elastic_password.txt](./vagrant/provision/siem/elastic_password.txt).
* Utilizar el script [runonce.sh](./vagrant/provision/runonce.sh) para que se actualicen los enrutados después del reinicio de la máquina.

### Script "install_honeypot.sh"

El script [install_honeypot.sh](./vagrant/provision/honeypot/install_honeypot.sh) se ejecuta una única vez en el primer arranque de la máquina virtual con Vagrant. Lo que hace es:

* Inicializar el sistema con [init_system.sh](./vagrant/provision/init_system.sh).
* Configurar las interfaces de red. La configuración no se puede aplicar inmediatamente porque se interrumpe la interacción con Vagrant al reiniciar las interfaces de red. Los cambios estarán operativos después del siguiente inicio del sistema.
* Instalar Docker con [install_docker.sh](./vagrant/provision/install_docker.sh).
* Instalar Suricata con [install_suricata.sh](./vagrant/provision/install_suricata.sh).
* Instalar el [agente de Elasticsearch](./vagrant/provision/elastic-agent-8.0.1-linux-x86_64.tar.gz) con [install_elastic_agent.sh](./vagrant/provision/install_elastic_agent.sh). Este script se apoya en el archivo de configuración previamente guardado por el usuario en la ubicación [\vagrant\provision\elastic-agent.yml](./vagrant/provision/elastic-agent.yml).
* Arrancar servicio SSH como Honeypot en el puerto 3333 utilizando Docker.
* Utilizar el script [runonce.sh](./vagrant/provision/runonce.sh) para que se actualicen los enrutados después del reinicio de la máquina.

### Script "install_workstation.sh"

El script [install_workstation.sh](./vagrant/provision/workstation/install_workstation.sh) se ejecuta una única vez en el primer arranque de la máquina virtual con Vagrant. Lo que hace es:

* Inicializar el sistema con [init_system.sh](./vagrant/provision/init_system.sh).
* Configurar las interfaces de red. La configuración no se puede aplicar inmediatamente porque se interrumpe la interacción con Vagrant al reiniciar las interfaces de red. Los cambios estarán operativos después del siguiente inicio del sistema.
* Instalar Docker con [install_docker.sh](./vagrant/provision/install_docker.sh).
* Instalar Suricata con [install_suricata.sh](./vagrant/provision/install_suricata.sh).
* Instalar el [agente de Elasticsearch](./vagrant/provision/elastic-agent-8.0.1-linux-x86_64.tar.gz) con [install_elastic_agent.sh](./vagrant/provision/install_elastic_agent.sh). Este script se apoya en el archivo de configuración previamente guardado por el usuario en la ubicación [\vagrant\provision\elastic-agent.yml](./vagrant/provision/elastic-agent.yml).
* Utilizar el script [runonce.sh](./vagrant/provision/runonce.sh) para que se actualicen los enrutados después del reinicio de la máquina.

### Script "init_system.sh"

El script común [init_system.sh](./vagrant/provision/init_system.sh) inicializa la máquina virtual para su uso en el laboratorio. Lo que hace es:

* Mejorar la apariencia del Shell utilizando colores.
* Establecer la zona horaria de "Europe/Madrid".
* Crear usuario "*devops*" con contraseña "devops" y añadirlo en el grupo de sudoers sin contraseña. Esto es un fallo de seguridad, pero el uso de este usuario es para pruebas en el laboratorio, y está fuera del alcance del ejercicio.

### Script "install_docker.sh"

El script común [install_docker.sh](./vagrant/provision/install_docker.sh) prepara Docker para su uso en la máquina virtual. Lo que hace es:

* Instalar Docker.
* Añadir el usuario "*devops*" al grupo "*docker*". Esto permite que el usuario "*devops*" pueda ejecutar Docker.

### Script "install_suricata.sh"

El script común [install_suricata.sh](./vagrant/provision/install_suricata.sh) prepara Suricata para su uso en la máquina virtual. Lo que hace es:

* Instalar Suricata.
* Arrancar el servicio de Suricata.

### Script "install_elastic_agent.sh"

El script común [install_elastic_agent.sh](./vagrant/provision/install_elastic_agent.sh) prepara el agente de Elasticsearch para su uso en la máquina virtual. Lo que hace es:

* Descromprimir el paquete del [agente de Elasticsearch](./vagrant/provision/elastic-agent-8.0.1-linux-x86_64.tar.gz).
* Utilizar el archivo de configuración [\vagrant\provision\elastic-agent.yml](./vagrant/provision/elastic-agent.yml) previamente guardado por el usuario, estableciendo:
  * El usuario "*elastic*" que utiliza el agente.
  * La contraseña del usuario "*elastic*" almacenada durante el aprovisionamiento de la máquina SIEM en el archivo [\vagrant\provision\siem\elastic_password.txt](./vagrant/provision/siem/elastic_password.txt).
  * La IP 192.168.200.2 de la máquina SIEM.
* Instalar el servicio del agente de Elasticsearch.

### Script "runonce.sh"

El script común [runonce.sh](./vagrant/provision/runonce.sh) ejecuta un comando la próxima vez que se inicie la máquina. Fue obtenido de este [hilo de serverfault.com](https://serverfault.com/questions/148341/linux-schedule-command-to-run-once-after-reboot-runonce-equivalent).

## Información de interés

No se realiza el montaje de la carpeta con los scripts de aprovisionamiento (```/vagrant```) cuando se arranca una máquina virtual manualmente utlizando Virtual Box en lugar de Vagrant.

## Mejoras para el futuro

* Utilizar máquina UTM como servidor DNS.
* Evitar la interacción con el usuario para obtener el archivo de configuración del agente de Elasticseach (por ejemplo utilizando "curl").

## Tear Down

Para eliminar todos los recursos del laboratorio se puede ejecutar:

```bash
vagrant destroy -f
```

## Corrección del profesor

No puedo decirte otra cosa que enhorabuena por tu trabajo, le has dado mucha calidad al mismo automatizando todo y utilizando todas las partes posibles e incluso cambiando algunas de ellas como el UTM.

Sólo puedo decirte que muy buen trabajo y que la documentación podía ser de otra manera, pero creo que es un acierto el hacerlo con los comentarios en el código.

¡Enhorabuena!
