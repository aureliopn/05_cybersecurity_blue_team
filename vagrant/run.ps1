vagrant up utm | Tee-Object -file utm_up.log
vagrant up siem | Tee-Object -file siem_up.log

Write-Output "_________________________________________________________________________________________________________"
Write-Output "1. Log in 'http://localhost:5601/' (ckeck port if you have changed it) using user 'elastic' and password:"
Get-Content -Path .\provision\siem\elastic_password.txt | Write-Output
Write-Output "2. Add Suricata Events integrations."
Write-Output "3. Save the standalone configuration file to '.\provision\elastic-agent.yml'"
Write-Output "_________________________________________________________________________________________________________"
Read-Host "Press ENTER when done to continue..."

vagrant up honeypot | Tee-Object -file honeypot_up.log
vagrant up workstation | Tee-Object -file workstation_up.log

vagrant reload | Tee-Object -file reload.log
